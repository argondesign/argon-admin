<?php

//------------------------------------------------------------------------------
// Email Settings
//------------------------------------------------------------------------------

// Disable auto-update email notifications for plugins
add_filter( 'auto_plugin_update_send_email', '__return_false' );

// Disable auto-update email notifications for themes
add_filter( 'auto_theme_update_send_email', '__return_false' );

// Send recovery mode email to Argon support
add_filter( 'recovery_mode_email', function( $email ) {

	$email['to'] = 'support@argon.com.au';
	
	return $email;

});

//------------------------------------------------------------------------------
// Remove Admin Dashboard Items
//------------------------------------------------------------------------------

add_action( 'wp_dashboard_setup', 'argonadmin_remove_dashboard_widgets' );

function argonadmin_remove_dashboard_widgets() {

	$current_user = wp_get_current_user();

	remove_meta_box( 'dashboard_php_nag', 'dashboard', 'normal' );

	if ( $current_user->user_login != 'argonadmin' ) {

		remove_meta_box( 'dashboard_site_health', 'dashboard', 'normal' );

	}

}

//------------------------------------------------------------------------------
// Admin Menu Items
//------------------------------------------------------------------------------

add_action( 'admin_menu', 'argonadmin_menu_pages' );

function argonadmin_menu_pages() {

	$current_user = wp_get_current_user();

	remove_menu_page( 'wpstaq-main.php' );

	if ( $current_user->user_login != 'argonadmin' ) {

		remove_submenu_page( 'index.php', 'update-core.php' );
		remove_submenu_page( 'tools.php', 'site-health.php' );

	}

}

add_action( 'wp_before_admin_bar_render', 'argonadmin_admin_bar_links' );

function argonadmin_admin_bar_links() {

	global $wp_admin_bar;

	$current_user = wp_get_current_user();

	if ( $current_user->user_login != 'argonadmin' ) {

		$wp_admin_bar->remove_menu( 'updates' );

	}

}

//------------------------------------------------------------------------------
// Remove Update Nags
//------------------------------------------------------------------------------

add_action( 'admin_head', 'argonadmin_remove_nags' );

function argonadmin_remove_nags() {

	$current_user = wp_get_current_user();

	if ( $current_user->user_login != 'argonadmin' ) {

		remove_action( 'admin_notices', 'update_nag', 3 );
		remove_action( 'admin_notices', 'maintenance_nag', 10 );

	}

}

//------------------------------------------------------------------------------
// Override/Hide Staq Branding
//------------------------------------------------------------------------------

add_action( 'admin_head', 'argonadmin_hide_staq' );
add_action( 'wp_footer', 'argonadmin_hide_staq', 999 );

function argonadmin_hide_staq() {

	if ( is_user_logged_in() && defined( 'WPSH_PLUGIN_VERSION' ) ) {
		
		?>

		<style>
		.wpstaq .wpstaq-logo { background: url(<?php echo plugin_dir_url( __FILE__ ); ?>assets/img/argonlogo.svg) no-repeat center center / 75% auto; }
		.wpstaq .wpstaq-logo img { opacity: 0; }
		</style>

		<script>
			window.onload = function() {
				if (window.jQuery) {
					jQuery('#wp-admin-bar-wpstaq-topbar > a').text('Hosting');
				}
			};
		</script>

		<?php

	}

}

//------------------------------------------------------------------------------
// ACF - Disable HTML escaping for the_field()
//------------------------------------------------------------------------------

add_filter( 'acf/admin/prevent_escaped_html_notice', '__return_true' );
add_filter( 'acf/shortcode/allow_unsafe_html', '__return_true' );
add_filter( 'acf/the_field/allow_unsafe_html', '__return_true' );

//------------------------------------------------------------------------------
// Remove sizes=auto on loazyloaded images
//------------------------------------------------------------------------------

// https://make.wordpress.org/core/2024/10/18/auto-sizes-for-lazy-loaded-images-in-wordpress-6-7/

add_filter( 'wp_content_img_tag', function ( $image ) {
	
	return str_replace( ' sizes="auto, ', ' sizes="', $image );

});

add_filter( 'wp_get_attachment_image_attributes', function ( $attr ) {

	if ( isset( $attr['sizes'] ) ) {
	
		$attr['sizes'] = preg_replace( '/^auto, /', '', $attr['sizes'] );

	}

	return $attr;

});