<?php

/*
Plugin Name: Argon Admin
Plugin URI: https://www.argondesign.com.au
Description: This plugin allows Argon to apply important updates across all client websites.
Version: 1.8
Author: Argon Design
Author URI: https://www.argondesign.com.au
*/

if ( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'ArgonAdmin' ) ) :

class ArgonAdmin {

	/** @var string The plugin version number. */
	var $version = '1.8';

	/**
	 * __construct
	 *
	 * A dummy constructor to ensure ArgonAdmin is only setup once.
	 *
	 * @since	1.0
	 *
	 * @param	void
	 * @return	void
	 */	
	function __construct() {
		// Do nothing.
	}
	
	/**
	 * initialize
	 *
	 * Sets up the Argon Admin plugin.
	 *
	 * @since	1.0
	 *
	 * @param	void
	 * @return	void
	 */
	function initialize() {

		add_action( 'init', array( $this, 'init' ) );

	}

	/**
	 * init
	 *
	 * Completes the setup process on "init" of earlier.
	 *
	 * @since	1.0
	 *
	 * @param	void
	 * @return	void
	 */
	function init() {

		// Add our functions.php

		require( 'functions.php' );

		// Check for updates

		$this->updateCheck();

	}

	/**
	 * Update Checker
	 *
	 * Check the Argon server for updates.
	 *
	 * @since	1.0
	 *
	 * @param	void
	 * @return	void
	 */
	function updateCheck() {

		require_once( 'plugin-update-checker/plugin-update-checker.php' );

		$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(

			'https://bitbucket.org/argondesign/argon-admin',
			__FILE__,
			'argon-admin'
		
		);
		
		$myUpdateChecker->setBranch( 'master' );

	}

}

function argonAdmin() {

	global $argonAdmin;
	
	// Instantiate only once

	if ( !isset( $argonAdmin ) ) {

		$argonAdmin = new ArgonAdmin();
		$argonAdmin->initialize();
	
	}
	
	return $argonAdmin;

}

// Instantiate

argonAdmin();

endif;

?>