=== Argon Admin ===
Stable tag: v1.8
Requires at least: 3.0
Tested up to: 6.7.1
Requires PHP: 5.6.0

== Description ==

This plugin allows Argon to apply important updates across all client websites.

== Changelog ==
 
= 1.0 =
* Plugin creation.

= 1.1 =
* Added description to readme.

= 1.2 =
* Send recovery mode emails to Argon support.

= 1.3 =
* Hide WP updates and Staq links

= 1.4 =
* Admin styling customisations

= 1.5 =
* Admin styling customisations

= 1.6 =
* Admin styling customisations

= 1.7 =
* ACF security updates

= 1.8 =
* Fixed an image sizing issue after WP update which added auto sizes for lazy loaded images
https://make.wordpress.org/core/2024/10/18/auto-sizes-for-lazy-loaded-images-in-wordpress-6-7/